import * as cdk from '@aws-cdk/core'
import { CfnCacheCluster, CfnCacheClusterProps } from '@aws-cdk/aws-elasticache'

export class InfrastructureSharedStack extends cdk.Stack {
  props: any

  constructor(scope: cdk.Construct, id: string, props: any, stackProps?: cdk.StackProps) {
    super(scope, id, stackProps)

    this.props = props

    new CfnCacheCluster(this, 'redis-cluster', <CfnCacheClusterProps>{
      cacheNodeType: 'cache.t2.micro',
      engine: 'redis',
      numCacheNodes: 1,
      clusterName: 'redis-cluster',
      port: 6379,
      vpcSecurityGroupIds: [props['securityGroup']],
    })
  }
}
