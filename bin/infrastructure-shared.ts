#!/usr/bin/env node
import "source-map-support/register";
import * as cdk from "@aws-cdk/core";
import { InfrastructureSharedStack } from "../lib/infrastructure-shared-stack";

const app = new cdk.App();
const props = app.node.tryGetContext("default");

new InfrastructureSharedStack(app, "shared", props, {
  env: { account: props["account"], region: props["region"] },
});
